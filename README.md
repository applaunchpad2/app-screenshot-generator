# App Screenshot Generator

Design tool to create beautiful app screenshots for App Store and Google Play pages.  
Website: https://theapplaunchpad.com  
Mockup Generator: https://theapplaunchpad.com/mockup-generator  